## Home assignment

## Tech stack
+ Java 11
+ Spring-boot 2
+ H2 DB
+ JUnit 5
+ Mockito
+ lombok
+ Swagger

## Requirement
1. Java
2. Maven
3. Port 8081 should be open

## Run tests
```bash
mvn clean test
```

## Run app
```bash
mvn spring-boot:run
```
App will be run on port 8081.

URL: http://localhost:8081

## Rest API Docs
+ [Swagger docs](http://localhost:8081/swagger-ui.html)