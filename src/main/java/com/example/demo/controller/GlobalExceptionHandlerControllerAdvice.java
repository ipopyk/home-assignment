package com.example.demo.controller;

import com.example.demo.model.dto.ApiError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Objects;

@Slf4j
@RestController
@ControllerAdvice
public class GlobalExceptionHandlerControllerAdvice {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ApiError> handleIllegalArgumentException(IllegalArgumentException e,  HttpServletRequest request) {
        return ResponseEntity.badRequest().body(buildError(e, 400, request));
    }

    private ApiError buildError(Exception e, int errorCode,  HttpServletRequest request) {
        return ApiError.builder()
                .status(errorCode)
                .message(Objects.nonNull(e) ? e.getMessage() : "")
                .path(request.getServletPath())
                .timestamp(LocalDateTime.now())
                .build();
    }
}
