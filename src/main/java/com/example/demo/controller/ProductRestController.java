package com.example.demo.controller;

import com.example.demo.model.dto.AddProductDto;
import com.example.demo.model.dto.UpdateProductDto;
import com.example.demo.model.entity.Product;
import com.example.demo.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/v1")
public class ProductRestController {
    private final ProductService service;

    public ProductRestController(ProductService service) {
        this.service = Objects.requireNonNull(service);
    }

    @ApiOperation(value = "Get list of all products", response = Product.class)
    @GetMapping(value = "/products")
    public ResponseEntity<List<Product>> getProducts() {
        return ResponseEntity.ok(service.getAll());
    }

    @ApiOperation(value = "Create new product", response = Product.class)
    @PostMapping(value = "/product")
    public ResponseEntity<Product> add(@Valid @RequestBody
                                       @ApiParam(name = "product", value = "product[name, price]") AddProductDto dto) {
        Product product = service.add(dto);
        return ResponseEntity.created(URI.create("/api/v1/products/" + product.getId())).body(product);
    }

    @ApiOperation(value = "Update product", response = Product.class)
    @PutMapping(value = "/products/{id}")
    public ResponseEntity<Product> update(@PathVariable Long id,
                                          @ApiParam(name = "product", value = "product[name, price]")
                                          @RequestBody UpdateProductDto dto) {
        Product product = service.update(id, dto);
        return ResponseEntity.ok().body(product);
    }

    @ApiOperation(value = "Delete product")
    @DeleteMapping(value = "/products/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }


}
