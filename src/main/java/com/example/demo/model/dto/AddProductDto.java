package com.example.demo.model.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class AddProductDto {
    @ApiParam(value = "Product name", required = true)
    @NotEmpty
    private String name;
    @ApiParam(value = "Product price", required = true)
    @NotNull
    private Integer price;
}
