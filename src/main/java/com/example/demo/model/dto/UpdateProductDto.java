package com.example.demo.model.dto;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class UpdateProductDto {
    @ApiParam(value = "Product name")
    private String name;
    @ApiParam(value = "Product price")
    private Integer price;
}
