package com.example.demo.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product")
@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@Where(clause = "deleted=false")
@SequenceGenerator(name = "PRODUCT_SEQUENCE", sequenceName = "PRODUCT_SEQUENCE")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product extends Auditable<String> implements SoftDelete {
    @Id
    @GeneratedValue(generator = "PRODUCT_SEQUENCE")
    private Long id;
    @NotEmpty
    private String name;
    @NotNull
    private Integer price;
    @JsonIgnore
    private boolean deleted = Boolean.FALSE;

    @Override
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
