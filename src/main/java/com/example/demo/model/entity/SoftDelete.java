package com.example.demo.model.entity;

public interface SoftDelete {
    void setDeleted(Boolean deleted);
}
