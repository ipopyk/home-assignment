package com.example.demo.service;

import com.example.demo.model.dto.AddProductDto;
import com.example.demo.model.dto.UpdateProductDto;
import com.example.demo.model.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Product add(AddProductDto product);

    Product update(Long id, UpdateProductDto product);

    List<Product> getAll();

    Optional<Product> get(Long id);

    Product delete(Long id);

}
