package com.example.demo.service.impl;

import com.example.demo.model.dto.AddProductDto;
import com.example.demo.model.dto.UpdateProductDto;
import com.example.demo.model.entity.Product;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;

    public ProductServiceImpl(ProductRepository repository) {
        this.repository = Objects.requireNonNull(repository);
    }

    @Override
    public Product add(AddProductDto productDto) {
        Objects.requireNonNull(productDto);
        Product product = Product.builder().name(productDto.getName()).price(Integer.valueOf(productDto.getPrice())).build();
        return repository.save(product);
    }

    @Override
    public Product update(Long id, UpdateProductDto product) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(product);
        return repository.findById(id)
                .map(p->{
                    if (Objects.nonNull(product.getName())) {
                        p.setName(product.getName());
                    }
                    if (Objects.nonNull(product.getPrice())) {
                        p.setPrice(Integer.valueOf(product.getPrice()));
                    }
                    if (Objects.nonNull(product.getName()) || Objects.nonNull(product.getPrice())) {
                        return repository.save(p);
                    }
                    return p;
                }).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<Product> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Product> get(Long id) {
        Objects.requireNonNull(id);
        return repository.findById(id);
    }

    @Override
    public Product delete(Long id) {
        Objects.requireNonNull(id);
        return repository.findById(id)
                .map(product -> {
                    if (!product.isDeleted()) {
                        product.setDeleted(Boolean.TRUE);
                        return repository.save(product);
                    }
                    return product;
                }).orElseThrow(IllegalArgumentException::new);
    }
}
