package com.example.demo.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestInstance(PER_CLASS)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
@ContextConfiguration
@WebAppConfiguration
public class ProductRestControllerIntegrationTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @BeforeAll
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void givenWac_whenServletContext_thenItProvidesDiffController() {
        ServletContext servletContext = wac.getServletContext();
        assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        assertNotNull(wac.getBean("productRestController"));
    }

    @Test
    public void givenValidData_addProduct_productShouldBeAddedAndReturn201Status() throws Exception {
        mockMvc.perform(post("/api/v1/product")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"name\":\"tesla\",\"price\":\"100\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/products/")))
                .andExpect(jsonPath("$.name").value("tesla"))
                .andExpect(jsonPath("$.price").value(100));
    }

    @Test
    public void givenNotExistsProductId_deleteProduct_shouldReturnBadRequest() throws Exception {
        mockMvc.perform(delete("/api/v1/products/9999"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void addTwoProductsAndGetAllProducts_shouldReturn200StatusAndContainAddedProductsInBody() throws Exception {
        MvcResult apple = mockMvc.perform(post("/api/v1/product")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"name\":\"apple\",\"price\":\"11\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/products/")))
                .andExpect(jsonPath("$.name").value("apple"))
                .andExpect(jsonPath("$.price").value(11)).andReturn();

        MvcResult orange = mockMvc.perform(post("/api/v1/product")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content("{\"name\":\"orange\",\"price\":\"12\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, containsString("/api/v1/products/")))
                .andExpect(jsonPath("$.name").value("orange"))
                .andExpect(jsonPath("$.price").value(12)).andReturn();


        MvcResult mvcResult = mockMvc.perform(get("/api/v1/products"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(mvcResult.getResponse().getContentAsString().contains(orange.getResponse().getContentAsString()));
        assertTrue(mvcResult.getResponse().getContentAsString().contains(apple.getResponse().getContentAsString()));
    }
}
