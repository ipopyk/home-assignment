package com.example.demo.controller;

import com.example.demo.model.dto.AddProductDto;
import com.example.demo.model.dto.UpdateProductDto;
import com.example.demo.model.entity.Product;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductRestControllerTest {
    @Mock
    private ProductService productService;

    private MockMvc mockMvc;

    private List<Product> products = asList(Product.builder().id(1L).name("bmw").price(100).build(),
                                            Product.builder().id(1L).name("tesla").price(200).build());

    private Product createdProduct = Product.builder().id(1L).name("tesla").price(300).deleted(false).build();

    @BeforeAll
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new ProductRestController(productService))
                .build();
        when(productService.getAll()).thenReturn(products);
        when(productService.add(any(AddProductDto.class))).thenReturn(createdProduct);
    }

    @Test
    public void whenGetAllProducts_returnAllProducts() throws Exception {
        mockMvc.perform(get("/api/v1/products").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(jsonPath("$[0].name").value("bmw"))
                .andExpect(jsonPath("$[0].price").value(100))
                .andExpect(jsonPath("$[1].name").value("tesla"))
                .andExpect(jsonPath("$[1].price").value(200));
    }

    @Test
    public void shouldAddProduct_whenValidRequest() throws Exception {
        mockMvc.perform(post("/api/v1/product")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"tesla\", \"price\": \"300\"}"))
        .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(header().string("Location", "/api/v1/products/1"))
                .andExpect(jsonPath("$.name").value("tesla"))
                .andExpect(jsonPath("$.price").value(300));
    }

    @Test
    public void shouldUpdateProduct_whenProvidedProperDataWithRequest() throws Exception {
        Product product = Product.builder().id(1L).name("bmw").price(300).build();
        Mockito.lenient().when(productService.update(anyLong(), any(UpdateProductDto.class))).thenReturn(product);
        mockMvc.perform(put("/api/v1/products/1")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .characterEncoding("utf-8")
                .content("{\"name\": \"bmw\", \"price\": \"300\"}"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldDeleteProduct_whenProvidedProductIdThatExists() throws Exception {
        mockMvc.perform(delete("/api/v1/products/1").accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
