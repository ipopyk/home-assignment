package com.example.demo.service;

import com.example.demo.model.dto.AddProductDto;
import com.example.demo.model.dto.UpdateProductDto;
import com.example.demo.model.entity.Product;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductServiceTest {

    @Mock
    private ProductRepository repository;

    private ProductService service;

    private Optional<Product> product;
    private Optional<Product> emptyProduct;
    private List<Product> products;
    private Product p1 = Product.builder().id(2L).name("audi").price(200).build();
    private Product p2 = Product.builder().id(3L).name("bmw").price(300).build();
    private AddProductDto productDto;
    private UpdateProductDto updateProductDto;
    private Product savedProduct;
    private Product updatedProduct;
    private Product deletedProduct;
    private Product deletedProductObj;
    private Product notDeletedProductObj;


    @BeforeAll
    public void init() {
        service = new ProductServiceImpl(repository);
        product = Optional.of(Product.builder().name("tesla").price(100).build());
        deletedProductObj = Product.builder().id(333L).name("tesla").price(100).deleted(Boolean.TRUE).build();
        notDeletedProductObj = Product.builder().id(333L).name("tesla").price(100).deleted(Boolean.FALSE).build();
        products = Arrays.asList(p1, p2);
        emptyProduct = Optional.empty();
        productDto = new AddProductDto("renault", 100);
        updateProductDto = new UpdateProductDto("renault", 200);
        savedProduct = Product.builder().name("renault").price(100).build();
        updatedProduct = Product.builder().name("renault").price(200).build();
        deletedProduct = Product.builder().id(999L).name("acura").price(123).deleted(Boolean.TRUE).build();

        when(repository.save(ArgumentMatchers.any(Product.class))).thenReturn(deletedProductObj);
        when(repository.findById(anyLong())).thenReturn(Optional.empty());
        Mockito.lenient().when(repository.findById(1L)).thenReturn(product);
        when(repository.findAll()).thenReturn(products);
        Mockito.lenient().when(repository.save(savedProduct)).thenReturn(savedProduct);
        Mockito.lenient().when(repository.save(updatedProduct)).thenReturn(updatedProduct);
        Mockito.lenient().when(repository.findById(999L)).thenReturn(Optional.of(deletedProduct));
        Mockito.lenient().when(repository.findById(333L)).thenReturn(Optional.of(notDeletedProductObj));
    }

    @Test
    public void ifProductIdIsNull_ThrowException() {
        assertThrows(NullPointerException.class, ()-> service.get(null));
    }

    @Test
    public void ifProductWithGivenIdDoesNotExists_ReturnEmptyOptional() {
        assertThat(service.get(111L), equalTo(Optional.empty()));
    }

    @Test
    public void ifProductWithGivenIdExists_ReturnProduct() {
        assertThat(service.get(1L), equalTo(product));
    }

    @Test
    public void ifProductsExists_ReturnAllProducts() {
        List<Product> result = service.getAll();
        assertThat(result, hasItems(p1, p2));
        assertThat(result, hasSize(2));
    }

    @Test
    public void givenProductDto_saveToDB_returnSavedProduct() {
        assertThat(service.add(productDto), equalTo(savedProduct));
    }

    @Test
    public void givenUpdateProductDto_updateProduct_returnUpdatedProduct() {
        assertThat(service.update(1L, updateProductDto), equalTo(updatedProduct));
    }

    @Test
    public void givenUpdateProductDtoAndNotExistsProductId_updateProduct_throwIllegalArgumentException() {
        Mockito.lenient().when(repository.findById(111L)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class,
                ()->service.update(111L, updateProductDto));
    }

    @Test
    public void givenNullIProductId_updateProduct_throwNullPointerException() {
        assertThrows(NullPointerException.class,
                ()->service.update(null, updateProductDto));
    }

    @Test
    public void givenNullIdProduct_deleteProduct_throwNullPointerException() {
        assertThrows(NullPointerException.class,
                ()->service.delete(null));
    }

    @Test
    public void givenProductIdForNotExistsProduct_deleteProduct_throwIllegalArgumentException() {
        Mockito.lenient().when(repository.findById(111L)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class,
                ()->service.delete(111L));
    }

    @Test
    public void givenProductIdForAlreadyDeletedProduct_deleteProduct_shouldNotSaveProductToDb() {
        service.delete(999L);
        verify(repository, times(0)).save(deletedProduct);
    }

    @Test
    public void givenValidProductId_deleteProduct_shouldDeleteProduct() {
        assertEquals(deletedProductObj, service.delete(333L));
    }

}
